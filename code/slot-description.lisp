#|
    Copyright (C) 2021 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:hopes-and-dreams)

;;; FIXME! It is important that allocation (on the map or instance)
;;; be described differently to assignability.
;;; Just because something is not assignable on the instance
;;; it does not mean it is not assignable when it is in the
;;; class position. It is misleading and will kill someone.

(defgeneric evaluate-message (slot-description object receiver machine))
(defgeneric message-selector (slot-description))
(defgeneric slot-assignable-p (slot-description))
(defgeneric slot-parent-p (slot-description))
(defgeneric slot-lexicla-parent-p (slot-description))
(defgeneric slot-lexical-access-control (slot-description))
(defclass slot-description () ())

(defclass standard-slot-description (slot-description)
  ((%message-selector
    :initarg :message-selector :reader message-selector)
   (%slot-assignable-p
    :initarg :slot-assignable-p :reader slot-assignable-p
    :type boolean :initform nil)
   (%slot-parent-p
    :initarg :slot-parent-p :reader slot-parent-p
    :reader slot-parent-p :type boolean :initform nil)
   (%slot-lexical-parent-p
    :initarg :slot-lexical-parent-p :reader slot-lexical-parent-p
    :type boolean :initform nil)
   (%lexical-access-control
    :initarg :lexical-access-control :reader slot-lexical-access-control
    :type keyword :initform :private)))

(defgeneric slot-location-value (allocated-slotd receiver))
(defgeneric (setf slot-location-value)
    (new-value allocated-slotd object))
(defgeneric slot-location (allocated-slotd))
(defclass allocated-slot-description (standard-slot-description)
  ((%slot-location :initarg :slot-location :accessor slot-location)))

(defmethod slot-location-value ((slot allocated-slot-description)
                                object )
  (if (slot-assignable-p slot)
      (aref (object-vector object) (slot-location slot))
      (slot-location slot)))

(defmethod (setf slot-location-value) (new-value
                                       (slot allocated-slot-description)
                                       object)
  (if (slot-assignable-p slot)
      (setf (aref (object-vector object) (slot-location slot))
            new-value)
      (setf (slot-location slot) new-value)))

(defmethod evaluate-message ((slot allocated-slot-description)
                             object
                             receiver
                             machine)
  (declare (ignore receiver))
  (push-data-stack (slot-location-value slot object) machine)
  nil)

;;; TODO: DEAR GOD CHANGE THIS UNBOUND.
(defun make-data-slot
    (selector &key assignablep (lexical-access-control :private)
                (initial-value 'unbound))
  (make-instance 'allocated-slot-description
                 :message-selector selector
                 :slot-assignable-p assignablep
                 :lexical-access-control lexical-access-control
                 :slot-location initial-value))

;;; assignment slot

(defclass assignment-slot-description (standard-slot-description)
  ((%target-slot-description
     :initarg :target-slot-description
     :reader target-slot-description)))

(defun make-assignment-slot (selector target)
  (make-instance
   'assignment-slot-description
   :target-slot-description target
   :message-selector selector))

(defmethod print-object ((object slot-description) stream)
  (print-unreadable-object (object stream :type t :identity t)
    (format stream "~a" (message-selector object))))

(defun make-lexical-parent-slot (lexical-parent)
  (make-instance
   'allocated-slot-description
   :message-selector (gensym)
   :slot-parent-p t
   :slot-assignable-p nil
   :slot-location lexical-parent
   :lexical-access-control :private))
