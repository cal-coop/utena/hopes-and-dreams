#|
    Copyright (C) 2022 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:hopes-and-dreams.object)


;;; use a generator cleiint in a dynamic variable
;;; to select the method implementation.
(defmacro define-cl-method (selector (&rest arguments)
                            &body body)
  `())

;;; The word base means nothing.
;;; We just need a class to start somewhere.
(defun class-definition-mixin-base-class ()
;;; well actually we want a system class
;;; that responds to a set of messages right?
;;; but why do we need a system class for this it can just be
;;; a standard class.
  (make-instance
   'utena-standard-object
   :effective-slots

   ))

;;; K, I've just decided supermix goes (nil mix mix) for no superclass
;;; and (S mix mix) otherwise.
(defun make-class-definition-mixin
    (constructor
     direct-slots
     &key super-mixin-precedence-list-block-template)
  (make-instance
   'utena-standard-object

   )
  )
