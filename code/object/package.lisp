#|
    Copyright (C) 2022 Gnuxie <Gnuxie@protonmail.com>
|#

(defpackage #:hopes-and-dreams.object
  (:use #:cl)
  (:local-nicknames
   (#:us #:utena-symbols))
  (:shadow #:class)
  (:export
   #:slot-allocation-protocol
   #:slot-allocation
   #:slot-assignable-p
   #:slot-selector
   #:slot-access

   #:utena-system-class
   #:class-effective-slots
   #:class-superclass
   #:class-mixins
   #:class-constructor
   #:utena-standard-class

   #:utena-standard-object
   #:class
   #:object-vector

   #:produce-class))
