#|
    Copyright (C) 2021 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:hopes-and-dreams.sourcerer)

(defun find-operators (name cst-body)
  (remove name
          (cst:listify cst-body)
          :test-not #'eql
          :key (alexandria:compose #'cst:raw #'cst:first)))

;;; The default constructor doesn't have a selector
;;; it's like funcallable instances.
;;; well maybe, the easiest way to do it is with special
;;; slots for classes.
;;; unfortunatley this requires a special slot
;;; on the platform object, not here.
;;; I think different slot types for dispatch are becomming a mistake
;;; IT's valid to want to assign to them to change behavior.
;;; 1 week later: NAA. You miread, read module as objects again
;;; ST must have some bullshit way to match keywords to messages.

(defun parse-constructor* (method-name arguments body)
  (let ((method (transform-method arguments body)))
    (hopes-and-dreams:make-method-slot
     method-name method)))

(defun parse-constructor (cst)
  (cst:db source (constructor-symbol method-name arguments . body) cst
    (declare (ignore constructor-symbol))
    (parse-constructor* method-name arguments body)))

(defun parse-method (cst)
  "Yeah it's the same as constructor for now, cba lwl."
  (parse-constructor cst))

(defun parse-class (cst)
  (cst:db source (class-symbol class-name super-mix slots . body) cst
    (declare (ignore class-symbol))
    (parse-class* class-name super-mix slots body)))

(defun parse-class* (class-name super-mix slots body)
  (declare (ignore class-name))
  (let ((classes (find-operators 'us:class body))
        (constructors (find-operators 'us:constructor body))
        (methods (find-operators 'us:method body)))
    (make-instance
     'hopes-and-dreams:class-definition
     :constructor
     ;; why is there more than one constructor??
     ;; annoying because well they're messages
     ;; on the class instance
     ;; technically you can have more than one if
     ;; you have canonical class allocated slots
     ;; which idk if we do.
     (first
      (loop for constructor in constructors
            collect (parse-constructor constructor)))
     :direct-slots
     `(,@ (loop for class in classes
                collect (make-instance
                         'h::allocated-slot-description
                         :message-selector (second class)
                         :slot-location
                         (apply #'parse-class (rest class))))

          ,@ (loop for method in methods
                   collect (parse-method method))
          ,@ (mapcan #'parse-slot (cst:listify slots)))
     :superclass-clause (parse-superclass-block super-mix)
     :mixin-precedence-list (parse-mixin-blocks super-mix))))

(defun parse-superclass-block (super-mix)
  "We're just going to say this is the first thing in the list,
otherwise it's just too horrible. If it is nil then we don't care."
  (if (cst:consp super-mix)
       ;; we need block syntax, but lets just pretend for now
       ;; that they're only single implicit self sends
      (h:make-block-template
       (transform-method '() (cst:first super-mix)))
      nil))

(defun parse-mixin-blocks (super-mix)
  (unless (cst:null super-mix)
    (mapcar
     (lambda (e)
       (h:make-block-template (transform-method '() e)))
     (cst:rest super-mix))))

(defun parse-slot (slot-form)
  (flet ((find-cst (thing)
           (cst:reconstruct thing slot-form nil)))
    (destructuring-bind (selector &key writable method)
        (mapcar #'cst:listify (cst:listify slot-form))
      (cond
        (method
         (let ((method-slot
                 (h:make-method-slot (find-cst selector) nil)))
           (if writable
               (list
                method-slot
                (h:make-assignment-slot
                 (find-cst `(us:set! ,selector))
                 method-slot))
               (list method-slot))))
        (t
         (let ((slot (h:make-data-slot (find-cst selector) :assignablep writable)))
           (if writable
               (list slot (h:make-assignment-slot (find-cst `(us:set! ,selector))
                                                  slot))
               (list slot))))))))
