#|
    Copyright (C) 2022 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:hopes-and-dreams.sourcerer)

;;;; This file is about macro and form expansion basically.
;;;; this is not for transforming source to other representations,
;;;; this is for expanding source before compiling.

;;;; We have made the mistake of making read macros happen after read
;;;; rather than during (see transform-expression).
;;;; So expression-transforms have to happen first
;;;; then macroexpansion.

(defvar *method-macros* (make-hash-table))

;;; lambda-list would have to accept a literal array
;;; probably some other context.
(defmacro define-method-macro (name (&rest lambda-list)
                               &body body)
  `(setf (gethash ',name *method-macros*)
         (lambda ,lambda-list ,@body)))

(defun method-macroexpand-1 (cst-expression)
  (let ((expression (cst:listify cst-expression)))
    (if (and (listp expression) (< 0 (length expression)))
        (destructuring-bind (call-position &rest arguments) expression
          (declare (ignore arguments))
          (let ((macro (gethash (cst:raw call-position) *method-macros*)))
            (if (null macro)
                (values cst-expression nil)
                (values
                 (funcall macro cst-expression)
                 t))))
        (values cst-expression nil))))

(defun method-macroexpand (expression)
  (loop for (next-expression expandedp)
          = (multiple-value-list
             (method-macroexpand-1 expression))
        while expandedp
        do (setf expression next-expression))
  expression)

(defun method-macrowalk (cst-body)
  (check-type cst-body cst:cst)
  (if (cst:consp cst-body)
      (cst:cstify
       (loop for expression in (cst:listify (method-macroexpand cst-body))
             collect (method-macrowalk expression))
       :source (cst:source cst-body ))
      cst-body))

;;; ok we fucked up, first of all the above isn't for transforming
;;; reader macros it's for fucking complicing the method ffs.
;;; anyways, then we still need cst with source,
;;; so solution is to pass cst and anywhere destructring happesn
;;; just use listfy and reconstruct via find-cst (which we will write)
;;; and fuck.
