#|
    Copyright (C) 2021 Gnuxie <Gnuxie@protonmail.com>
|#

(cl:in-package #:hopes-and-dreams.sourcerer)

(alexandria:define-constant +whitespace+ '(#\Space #\Newline #\Tab)
  :test #'equal)

(defun make-utena-symbol (string)
  (intern string (find-package 'utena-symbols)))

(defvar *document*)

;;; CST bits
(defun source-info (stream)
  (concrete:make-dumb-source-info *document* (file-position stream)))

(defun make-atom (atom source-info)
  (check-type atom (not cst:cst))
  (make-instance 'cst:atom-cst
                 :source source-info
                 :raw atom))

;;;; thanks hayley:
;;;; https://gist.github.com/no-defun-allowed/db8eb55c700c2fcea9a71fe7561453db
(defun read-list (stream &optional (end-char #\)))
  (if (char= (peek-char t stream) end-char)
      (let ((source-info (source-info stream)))
        ;; discard the end
        (read-char stream)
        (make-atom nil source-info))
      ;; we need the file position before we call cst:cons
      (let ((source-info (source-info stream)))
        (cst:cons (read-sexp stream)
                  (read-list stream end-char)
                  :source
                  source-info))))

(defun read-atom (stream &optional (collected nil))
  (let ((char (peek-char (null collected) stream nil :eof))
        (source-info (source-info stream)))
    (cond
      ((eql char :eof)
       (make-atom
        (make-utena-symbol
         (string-upcase
          (coerce (nreverse collected) 'string)))
        source-info))
      ((eql char #\:)
       (read-char stream)
       (let ((symbol
               (make-atom (make-utena-symbol
                           (string-upcase
                            (coerce (nreverse collected) 'string)))
                          source-info)))
         ;; we patch send later when it is in the complete list.
         ;; we can't patch it on the fly because (a foo: b) is not the same as
         ;; (foo: a b). or (a (send foo) b) and ((send foo) a b)
         ;; though i'm not sure what the former is suposed to be.
         (cst:reconstruct (list 'send symbol) symbol nil)))
      ((member char
               ' #. (append +whitespace+ '(#\( #\) #\[ #\]))
                 :test #'char=)
       (let ((string (coerce (nreverse collected) 'string)))
         (if (digit-char-p (elt string 0))
             (make-atom (parse-integer string) source-info)
             (make-atom
              (make-utena-symbol
               (string-upcase string))
              source-info))))
      (t
       (read-atom stream
                  (cons (read-char stream)
                        collected))))))

(defun handle-escape (stream)
  (let ((char (read-char stream)))
    (case char
      (#\" #\")
      (otherwise
       (error "Escape ~a not implemented" char)))))

(defun read-string (stream &optional source-info (collected nil))
  (let ((source-info (or source-info (source-info stream)))
        (char (read-char stream)))
    (case char
      (#\"
       (make-atom
        (coerce (nreverse collected) 'string)
        source-info))
      (#\Backslash
       (read-string stream
                    source-info
                    (cons (handle-escape stream)
                          collected)))
      (otherwise
       (read-string stream
                    source-info
                    (cons char collected))))))

(defun consume-whitespace (stream)
  (loop for char = (peek-char nil stream)
        while (member char '#.+whitespace+)
        do (read-char stream))
  nil)

;;; patch-send has to happen in read-list
;;; otherwise it breaks.
;;; this is doable because send is special privat symbol.
;;; and so should only appear in the send position
;;; even if read-list is recursive, we can trust it.

(defun read-sexp (stream)
  (consume-whitespace stream)
  (flet ((patch-send (cst-sexp)
           ;; send is just a bs sexpression we need to patch out,
           ;; not a CST
           (let* ((sexp (cst:listify cst-sexp))
                  (send-position (first sexp)))
             (if (and (cst:consp send-position)
                      (eql (cst:raw (cst:first send-position)) 'send))
                 (let ((source-info (cst:source (cst:second send-position))))
                   (cst:quasiquote
                    source-info
                    (send
                        (cst:second send-position)
                        (cst:unquote (second sexp))
                        (cst:unquote-splicing
                         (nthcdr 2 sexp))))
                   #+ (or)
                   `(,(make-atom 'send source-info)
                     (,(cst:second send-position)
                      ,(second sexp))
                     ;; Why does CST not have nthcons? and cddr etc.
                     ,@(nthcdr 2 sexp))
)
                 cst-sexp))))
    (let ((char (read-char stream)))
      (case char
        (#\(
         ;; CST:FIRST is stricter than CL:FIRST
         (let ((sexp (read-list stream)))
           (if (cst:consp sexp)
               (patch-send sexp)
               sexp)))
        (#\"
         (read-string stream (list char)))
        ;; keyword hack.
        (#\: (let ((source-info (source-info stream)))
               (make-atom
                (intern (symbol-name (cst:raw (read-atom stream nil)))
                        (find-package '#:keyword))
                source-info)))
        (#\[ (read-block stream))
        (#\' (let ((source-info (source-info stream)))
               (cst:cons (make-atom 'us:quote (source-info stream))
                         (read-sexp stream)
                         :source source-info)))
        (otherwise
         (read-atom stream (list char)))))))

(defun read-block (stream)
  (let ((source-info (source-info stream)))
    (cst:cons
     (make-atom 'us:block source-info)
     (read-list stream #\])
     :source source-info)))

(defun read-from-string* (string)
  (let ((*document* string))
    (with-input-from-string (stream string)
      (read-sexp stream))))

(defun read-from-file (stream)
  (read-sexp stream))

(defun read-from-file* (string)
  (with-input-from-string (stream string)
    (read-from-file stream)))
