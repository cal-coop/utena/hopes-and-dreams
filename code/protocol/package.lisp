#|
    Copyright (C) 2022 Gnuxie <Gnuxie@protonmail.com>
|#

(cl:in-package #:common-lisp-user)

(defpackage #:utena-symbols
  (:export
   #:self
   #:car
   #:cdr

   #:let
   ;; blocks
   #:block
   #:value

   #:class
   #:method
   #:constructor

   #:send
   #:quote
   #:set!))
