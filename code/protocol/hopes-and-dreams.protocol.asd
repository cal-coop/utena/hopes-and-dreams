(cl:in-package #:asdf-user)

(defsystem #:hopes-and-dreams.protocol
  :author "Gnuxie <gnuxie@applied-langua.ge>"
  :serial t
  :depends-on
  ("alexandria")
  :components
  ((:file "package")))
