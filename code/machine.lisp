#|
    Copyright (C) 2021 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:hopes-and-dreams)

(defgeneric utena-send (ec object selector &rest arguments))
(defmacro with-utena-send
    (result (object selector &rest arguments) (&key (protect-last-p t))
     &body body)
  (let* ((last-form (car (reverse body)))
         (protected-body
           (if protect-last-p
               body
               (reverse (cdr (reverse body)))))
         (protect-form
           `(alexandria:unwind-protect-case ()
                                            (progn ,@body)
              (:normal (error "You've fucked it mate, you've really gone and done it.")))))
    `(let ((,result (utena-send nil ,selector ,@arguments)))
       ;; there has to be a better way than what's happening with the
       ;; ifs here but whatever
       ,@(if protect-last-p
             (list protect-form)
             (list protect-form last-form)))))

(defmacro with-utena-sends
    (bindings (&rest keys &key (protect-last-p t)) &body body)
  `(with-utena-send ,(first bindings)
     ,@(if (null (rest bindings))
           body
           (list `(with-utena-send (rest bindings ,@body) ,keys)))))

(defgeneric push-data-stack (item machine))
(defgeneric pop-data-stack (machine))
(defgeneric foreign-send-message
    (object message-selector machine &rest arguments)
  (:documentation "Send a message to an object from CL
Useful for writing primitive methods but also "))
(defgeneric send-message (object message-selector machine))
(defgeneric find-constant (index machine))
;;; I REALLY DO NOT THINK THE APPLY-PREFIX WILL WORK LIKE THIS.
(defgeneric apply-prefix (machine))
(defgeneric (setf apply-prefix) (new-value machine))
(defclass machine () ())

(defclass standard-machine (machine)
  ((%data-stack :accessor data-stack :initform '())
   (%constant-table :accessor constant-table)
   (%activated-method
    :accessor activated-method :initform nil
    :documentation "NIL is used to mean the bottom of the stack.")))

(defmethod find-constant (index (machine standard-machine))
  (aref (constant-table machine) index))

(defmethod push-data-stack (item (machine standard-machine))
  (push item (data-stack machine)))

(defmethod pop-data-stack ((machine standard-machine))
  (pop (data-stack machine)))

(defmethod foreign-send-message
    (object selector (machine standard-machine) &rest arguments)
  ;; prepare for send.
  (loop for arg in (reverse arguments)
        do (push-data-stack arg machine))
  (push-data-stack object machine)
  (push-data-stack selector machine)
  ;; send.
  ;; in order fo this to work,
  ;; we basically have to have a way to send a specific instruction
  ;; to the machine just once, without entering the method evaluation loop.
  ;; and then return back here.
  ;; It's important for us to remember that the stack is represented
  ;; by return continuation spaghetti in the AR's, and we don't
  ;; need to wind the CL stack by recursing the method evaluation
  ;; loop for method calls.
  ;; get result.
  ;; This dispatch is for the send instruction.
  (let ((result (dispatch machine 3 0 (length arguments))))
    (unless (null result)
      (setf (activated-method machine) result)
      (dispatch-loop machine)))
  (pop-data-stack machine))
