(cl:in-package #:asdf-user)

(defsystem #:hopes-and-dreams.noir
  :author "Gnuxie <gnuxie@applied-langua.ge>"
  :serial t
  :components
  ((:file "package")
   (:file "noir")))
