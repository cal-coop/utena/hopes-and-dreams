(cl:in-package #:common-lisp-user)

(defpackage #:hopes-and-dreams
  (:use #:cl)
  (:local-nicknames
   (#:us #:utena-symbols)
   (#:o  #:hopes-and-dreams.object))
  (:shadow
   #:standard-object
   #:standard-class
   #:method)
  (:export
   ;; slot
   #:make-data-slot
   #:make-assignment-slot
   ;; method
   #:method
   #:make-argument-slot
   #:make-method-slot
   ;; block-template
   #:make-block-template
   ;; class
   #:class-definition
   ;; instructions
   #:make-literal-instruction
   #:make-implicit-self-send-instruction
   #:make-send-instruction
   ))
